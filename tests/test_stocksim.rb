require 'test/unit'
require "./lib/Stock.rb"
require "./lib/User.rb"
require "./lib/Exchange.rb"

class StockSimTest < Test::Unit::TestCase

	def test_creating_a_new_stock()
		newStock = Stock.new("NWSTK", 325.00)
		assert_equal("NWSTK", newStock.name)
		assert_equal(325.00, newStock.value)
	end

	def test_creating_a_new_user_with_no_stocks()
		newUser = User.new("newUser")
		assert_equal("newUser", newUser.name)
	end

	def test_creating_empty_exchange()
		newEx = Exchange.new("newEx", 10)
		assert_equal("newEx", newEx.name)
		assert_equal(10, newEx.size)
	end

	def test_creating_exchange_with_stocks()
		stock_ONR = Stock.new("ONR", 200.00)
		stock_TST = Stock.new("TST", 150.00)
		stock_LMR = Stock.new("LMR", 14.00)
		stocks = stock_ONR, stock_TST, stock_LMR
		newEx = Exchange.new("newEx", 10, stocks)
		assert_equal(stocks, newEx.stocks)
	end

		
end

