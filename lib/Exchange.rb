class Exchange

	attr_accessor :name, :size, :stocks

	def initialize(name, size, stocks = nil)
		@name = name
		@size = size
		@stocks = stocks
	end

end
